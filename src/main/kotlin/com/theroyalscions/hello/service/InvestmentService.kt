package com.theroyalscions.hello.service

import com.theroyalscions.hello.persistence.Investment
import com.theroyalscions.hello.persistence.InvestmentRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import kotlin.math.pow

@Service
class InvestmentService {
    @Autowired
    private lateinit var investmentRepository: InvestmentRepository

    @Value("\${hello-investment.interest-rate}")
    private lateinit var interestRate: String

    fun getAll(): Iterable<Investment> = investmentRepository.findAll()

    fun create(investment: Investment): Investment{
        calculateAmount(investment)
        return investmentRepository.save(investment)
    }

    private fun calculateAmount(investment: Investment) {
        investment.amount = investment.value * (1 + interestRate.toDoubleOrNull()!!).pow(investment.numberOfMonths.toInt())
    }

}