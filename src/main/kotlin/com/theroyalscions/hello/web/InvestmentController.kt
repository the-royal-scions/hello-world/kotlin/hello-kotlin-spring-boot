package com.theroyalscions.hello.web

import com.theroyalscions.hello.persistence.Investment
import com.theroyalscions.hello.service.InvestmentService
import com.theroyalscions.hello.web.dto.InvestmentPayload
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class InvestmentController {

    @Autowired
    lateinit var investmentService: InvestmentService

    @GetMapping("investment")
    fun getAllInvestments(): MutableList<InvestmentPayload> {
        val responses = arrayListOf<InvestmentPayload>()
        investmentService.getAll().forEach { responses.add(InvestmentPayload(it))}
        return responses
    }

    @PostMapping("investment")
    fun createInvestment(@RequestBody investmentPayload: InvestmentPayload): InvestmentPayload =
        InvestmentPayload(investmentService.create(investmentPayload.buildEntity()))
}