package com.theroyalscions.hello.web.dto

import com.theroyalscions.hello.persistence.Investment

data class InvestmentPayload(var value: Double, var numberOfMonths: Short, var amount: Double) {

    constructor(investment: Investment) : this(investment.value,investment.numberOfMonths,investment.amount)

    fun buildEntity(): Investment = Investment(value = this.value, numberOfMonths = this.numberOfMonths, amount = this.amount)
}