package com.theroyalscions.hello

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class HelloKotlinSpringBootApplication

//./gradlew bootRun
//Testando commit pelo mac VS Code
fun main(args: Array<String>) {
	runApplication<HelloKotlinSpringBootApplication>(*args)
}
