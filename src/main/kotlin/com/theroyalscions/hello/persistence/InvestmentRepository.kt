package com.theroyalscions.hello.persistence

import org.springframework.data.repository.CrudRepository

interface InvestmentRepository : CrudRepository<Investment, Int> {
}